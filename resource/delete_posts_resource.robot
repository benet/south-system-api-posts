*** Settings ***
Library  RequestsLibrary
Library  BuiltIn
Library  String
Library  Collections
Library  OperatingSystem
Library  FakerLibrary

*** Variables ***
${BASE_PATH}  /posts
${ID}  1
&{HEADERS}
...   Content-Type=application/json
...   charset=UTF-8


*** Keywords ***
Realizar request delete

  ${RESPONSE}=  DELETE Request  api_posts  ${BASE_PATH}/${ID}  data=${PAYLOAD}  headers=${HEADERS}
  Set Global Variable  ${RESPONSE}

Validar Status Code delete post

  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}

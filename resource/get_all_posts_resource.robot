*** Settings ***
Library  RequestsLibrary


*** Variables ***
${BASE_PATH}  /posts
${RESPONSE}


*** Keywords ***
Realizar request busca posts

  ${RESPONSE}=  GET Request  api_posts  ${BASE_PATH}
  Set Test Variable   ${RESPONSE}

Validar Status Code get post

  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}


Validar count posts

  ${count_posts}  Set Variable  100
  ${item_count}  Get Length  ${RESPONSE.json()}
  Should Be Equal As Integers  ${item_count}  ${count_posts}
  log  ${item_count}



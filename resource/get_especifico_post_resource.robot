*** Settings ***
Library  RequestsLibrary


*** Variables ***
${BASE_PATH}  /posts
${RESPONSE}


*** Keywords ***
Realizar request busca especifico post

  ${id}  Set Variable  2
  ${RESPONSE}=  GET Request  api_posts  ${BASE_PATH}/${id}
  Set Test Variable   ${RESPONSE}


Validar Status Code get especifico post

  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}


Validar Id post

  ${id}  Set Variable  2
  Should Be Equal As Strings   ${RESPONSE.json()['id']}  ${id}

Validar Title post

  ${title}  Set Variable  qui est esse
  Should Be Equal As Strings   ${RESPONSE.json()['title']}  ${title}

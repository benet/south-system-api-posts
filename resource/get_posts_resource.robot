*** Settings ***
Library  RequestsLibrary


*** Variables ***
${BASE_PATH}  /posts
${RESPONSE}


*** Keywords ***
Realizar request

  ${RESPONSE}=  GET Request  api_posts  ${BASE_PATH}
  Set Test Variable   ${RESPONSE}

Validar Status Code

  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}
*** Settings ***
Library  RequestsLibrary
Library  BuiltIn
Library  String
Library  Collections
Library  OperatingSystem
Library  FakerLibrary

*** Variables ***
${BASE_PATH}  /posts
${ID}  1
&{HEADERS}
...   Content-Type=application/json
...   charset=UTF-8


*** Keywords ***
Criar Patch_body

  ${title}=  FakerLibrary.country

  ${PAYLOAD}    Get File    ${CURDIR}/requests_json/patch_posts.json
  ${PAYLOAD}    Replace String  ${payload}  new_title      ${title}

  Set Global Variable  ${PAYLOAD}



Realizar Request Patch_schema
  ${RESPONSE}=  PATCH Request  api_posts  ${BASE_PATH}/${ID}  data=${PAYLOAD}  headers=${HEADERS}
  Set Global Variable  ${RESPONSE}


Validar Status Code atualizacao patch
  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}
*** Settings ***
Library  RequestsLibrary
Library  BuiltIn
Library  String
Library  Collections
Library  OperatingSystem
Library  FakerLibrary

*** Variables ***
${BASE_PATH}  /posts
${ID}  1
&{HEADERS}
...   Content-Type=application/json
...   charset=UTF-8


*** Keywords ***
Criar put_body

  ${PAYLOAD}    Get File    ${CURDIR}/requests_json/add_novo_post.json
  ${PAYLOAD}    Replace String  ${payload}  new_title      teste
  ${PAYLOAD}    Replace String  ${payload}  new_body       teste
  ${PAYLOAD}    Replace String  ${payload}  new_userId     1

  Set Global Variable  ${PAYLOAD}

Realizar request put

  ${RESPONSE}=  PUT Request  api_posts  ${BASE_PATH}/${ID}  data=${PAYLOAD}  headers=${HEADERS}
  Set Global Variable  ${RESPONSE}


Validar Status Code atualizacao put
  ${status_code}  Set Variable  200
  Should Be Equal As Strings   ${RESPONSE.status_code}  ${status_code}
*** Settings ***
Library  RequestsLibrary
Library  BuiltIn
Library  String
Library  Collections
Library  OperatingSystem
Library  FakerLibrary
Library  JSONSchemaLibrary


*** Variables ***
${BASE_PATH}  /posts

&{HEADERS}
...   Content-Type=application/json
...   charset=UTF-8


*** Keywords ***
Criar body_schema

  ${title}=  FakerLibrary.country
  ${body}=  FakerLibrary.country
  ${userId}=  FakerLibrary.Numerify

  ${PAYLOAD}    Get File    ${CURDIR}/requests_json/add_novo_post.json
  ${PAYLOAD}    Replace String  ${payload}  new_title      ${title}
  ${PAYLOAD}    Replace String  ${payload}  new_body       ${body}
  ${PAYLOAD}    Replace String  ${payload}  new_userId     ${userId}

  Set Global Variable  ${PAYLOAD}

Realizar request post_schema

  ${RESPONSE}=  POST Request  api_posts  ${BASE_PATH}  data=${PAYLOAD}  headers=${HEADERS}
  log  ${RESPONSE.json()}
  Set Global Variable  ${RESPONSE}


Validar schema

  Validate Json  post_schema.json  ${RESPONSE.json()}

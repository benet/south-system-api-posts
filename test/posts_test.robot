*** Settings ***
Documentation  Test API Rest: Posts
Resource  ../resource/get_posts_resource.robot
Resource  ../resource/post_posts_resource.robot
Resource  ../resource/put_posts_resource.robot
Resource  ../resource/patch_posts_resource.robot
Resource  ../resource/delete_posts_resource.robot
Resource  ../resource/schema_posts_resource.robot
Resource  ../resource/get_all_posts_resource.robot
Resource  ../resource/get_especifico_post_resource.robot


Suite Setup  Create Session  api_posts  ${BASE_URI}  verify=True

*** Variables ***
${BASE_URI}  https://jsonplaceholder.typicode.com


*** Test Cases ***
TC001: Validar status service
  [Tags]  status
  Realizar request
  Validar Status Code

TC002: Validar schema adicionar post
  [Tags]  contrato
  Criar body_schema
  Realizar request post_schema
  Validar schema


TC003: Validar inclusao novo post
  [Tags]  negocio
  Criar body
  Realizar request post
  Validar Status Code inclusao novo post


TC004: Validar atualizacao de post
  [Tags]  negocio
  Criar put_body
  Realizar request put
  Validar Status Code atualizacao put

TC005: Validar atualizacao parcial de post
  [Tags]  negocio
  Criar Patch_body
  Realizar Request Patch_schema
  Validar Status Code atualizacao patch


TC006: Validar exclusao de post
  [Tags]  negocio
  Realizar request delete
  Validar Status Code delete post


TC007: Validar busca todos posts
  [Tags]  negocio
  Realizar request busca posts
  Validar Status Code get post
  Validar count posts



TC008: Validar busca post
  [Tags]  negocio
  Realizar request busca especifico post
  Validar Status Code get especifico post
  Validar Id post
  Validar Title post






